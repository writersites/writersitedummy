---
title: Subscribe to receive notifications of new Stories
subtitle: Simple ways to get notifications of new content.
date: 2020-10-29
tags: ["Subscribe!", "news"]
summary: "You can get emails, follow on twitter, or use an rss feed."
uid: SUB201029
categories: ["dated"]
lastvote: 10423:12
upvotes: 0
downvotes: 0
popular: 0
---



Progress on technical matters is slow
While you're reading this, here's a short privacy statement. It's short because it's very simple - your email will only ever be used to send you messages.

